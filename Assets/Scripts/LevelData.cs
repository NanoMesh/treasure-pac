﻿using UnityEngine;
using System.Collections;

public class LevelData : MonoBehaviour
{
    public GameObject PlayerSpawnPoint;
    public GameObject[] EnemySpawnPoints;

    public GameObject GetPlayerSpawnPoint()
    {
        return PlayerSpawnPoint;
    }

    public GameObject[] GetEnemySpawnPoints()
    {
       return EnemySpawnPoints;
    }

}
