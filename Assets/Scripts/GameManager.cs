﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    /*
    Gamestate 
         0 = In Menus/Pause
         1 = In Game
         2 = Game Over
         3 = Victory
    */

    public static int gamestate;
    public static GameManager gameManagerInstance;
    public static int Score;
    public static int Bullets;
    public static int Lives;
    public static int CoinNum;
    

    public List<Vector3> coinPos;
    public List<Vector3> bulletPos;

    public GameObject[] spawnableThings;

    void Awake()
    {
        if(gameManagerInstance == null)
        {
            gameManagerInstance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
        
    }
    void Start()
    {
        UIManager.uiManagerInstance.UpdateUI();

    }
    public void ResetStats()
    {
        Score = 0;
        Bullets = 0;
        Lives = 3;
        CoinNum = 0;
        coinPos.Clear();
        UIManager.uiManagerInstance.UpdateUI();
    }

    void Update()
    {

        //DEBUG ALL THE STUFF
        if (Input.GetKeyDown(KeyCode.O))
        {
            RestartLevel();
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            gamestate++;
            Debug.Log("Gamestate is " + gamestate);
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            gamestate--;
            Debug.Log("Gamestate is " + gamestate);
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            UIManager.UIState++;
            Debug.Log("UIStat is " + UIManager.UIState);
            UIManager.uiManagerInstance.UpdateUI();
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            UIManager.UIState--;
            Debug.Log("UIState is " + UIManager.UIState);
            UIManager.uiManagerInstance.UpdateUI();
        }





    }
    public void AddDart()
    {
        Bullets++;
        UIManager.uiManagerInstance.UpdateUI();
    }

    public void LoseLife()
    {
        Lives--;
        if (Lives <= 0)
        {
            Debug.Log("GameOver;");
            gamestate = 2;
            UIManager.UIState = 5;
            UIManager.uiManagerInstance.UpdateUI();
        }
        else
        {
            RespawnPlayerAndEnemyes();
        }
        UIManager.uiManagerInstance.UpdateUI();
    }

    public void AddScore()
    {
        Score++;
        if (CoinNum <= 0)
        {
            Debug.Log("Victory;");
            gamestate = 3;
            UIManager.UIState = 4;
            UIManager.uiManagerInstance.UpdateUI();
        }

        UIManager.uiManagerInstance.UpdateUI();
    }

    public void RespawnPlayerAndEnemyes()
    {
        //Clears Enemyes and PacMans
        GameObject[] enemyesAlive = GameObject.FindGameObjectsWithTag("Enemy");
        GameObject[] pacsAlive = GameObject.FindGameObjectsWithTag("Player");
        if(enemyesAlive != null)
        { 
            for (int j = 0; j < enemyesAlive.Length; j++)
            {
                Destroy(enemyesAlive[j]);
            }
        }
        if (pacsAlive != null)
        { 
            for (int c = 0; c < pacsAlive.Length; c++)
            {
                Destroy(pacsAlive[c]);
            }
        }
       //Gets Spawn points 
       GameObject playerSpawn = GameObject.Find("LevelDataContainer").GetComponent<LevelData>().GetPlayerSpawnPoint();
       GameObject[] enemySpawns = GameObject.Find("LevelDataContainer").GetComponent<LevelData>().GetEnemySpawnPoints();
       GameObject playerClone = spawnableThings[0];
       playerClone = Instantiate(playerClone,playerSpawn.transform.position,Quaternion.Euler(90,0,0)) as GameObject;
    
       
        for (int i = 0; i < enemySpawns.Length; i++)
        {
            GameObject enemyClone = spawnableThings[i+2];
            enemyClone = Instantiate(enemyClone, enemySpawns[i].transform.position, Quaternion.Euler(90, 0, 0)) as GameObject;

        }
    }

    public void RestartLevel()
    {

        ResetCoins();
        ResetBullets();
        RespawnPlayerAndEnemyes();
        ResetStats();
        gamestate = 1;
        UIManager.UIState = 3;
        UIManager.uiManagerInstance.UpdateUI();
    }
    
    void ResetCoins()
    {
        GameObject[] coinsAlive = GameObject.FindGameObjectsWithTag("Coin");
        for (int j = 0; j < coinsAlive.Length; j++)
        {
            Destroy(coinsAlive[j]);
           
        }
        Debug.Log("Coins Destroyed");
        for (int c = 0; c < coinPos.ToArray().Length; c++)
        {
            GameObject coinClone = spawnableThings[1];
            coinClone = Instantiate(coinClone, coinPos[c], Quaternion.Euler(90, 0, 0)) as GameObject;
        }
        Debug.Log("Coins Spawned");
    }
    void ResetBullets()
    {
        GameObject[] bulletsAlive = GameObject.FindGameObjectsWithTag("Bullet");
        for (int b = 0; b < bulletsAlive.Length; b++)
        {
            Destroy(bulletsAlive[b]);
            
        }
        Debug.Log("Bullets Destroyed");
        for (int d = 0; d < bulletPos.ToArray().Length; d++)
        {
            GameObject bulletClone = spawnableThings[6];
            
            bulletClone = Instantiate(bulletClone, bulletPos[d], Quaternion.Euler(90, 270, 0)) as GameObject;
        }
        Debug.Log("Bullets Spawned");
    }
}
