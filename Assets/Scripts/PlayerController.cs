﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 5f;
    public Vector3 curDir;
    public Vector3 nextDir;

    private Rigidbody rigy;

    public enum MoveDirection {Up,Down,Left,Right};
    public MoveDirection moveDirection;
    public MoveDirection currentMoveDirection;
    public bool canMove;
    private Animator anim;

    public GameObject bullet;

    void Start ()
    {
        rigy = gameObject.GetComponent<Rigidbody>();
        anim = gameObject.GetComponent<Animator>();
        GetNextDir();
    }
	
    void Update()
    {
        if (GameManager.gamestate == 1)
        {
            if (Input.GetAxis("Horizontal") > 0)
            {
                if (CheckForClearTile(new Vector3(.75f, 0, 0)))
                {
                    moveDirection = MoveDirection.Right;
                    anim.SetFloat("animDir", 1f);
                }
            }
            else if (Input.GetAxis("Horizontal") < 0)
            {
                if (CheckForClearTile(new Vector3(-.75f, 0, 0)))
                {
                    moveDirection = MoveDirection.Left;
                    anim.SetFloat("animDir", .6f);
                }
            }
            else if (Input.GetAxis("Vertical") > 0)
            {
                if (CheckForClearTile(new Vector3(0, 0, .75f)))
                {
                    moveDirection = MoveDirection.Up;
                    anim.SetFloat("animDir", .3f);
                }
            }
            else if (Input.GetAxis("Vertical") < 0)
            {
                if (CheckForClearTile(new Vector3(0, 0, -.75f)))
                {
                    moveDirection = MoveDirection.Down;
                    anim.SetFloat("animDir", 0f);
                }

            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Shoot();
            }

        }
    }

   
	void FixedUpdate ()
    {
        if (GameManager.gamestate == 1)
        {

            curDir = transform.position;
            curDir.y = 0;


            if (canMove)
            {
                RaycastHit rayHit;
                if (Physics.Linecast(transform.position, nextDir, out rayHit))
                {
                    if (rayHit.transform.tag == "Wall")
                    {
                        Debug.DrawLine(transform.position, nextDir, Color.green);
                        canMove = false;
                    }
                }
                else
                {
                    canMove = true;
                }
                GetNextDir();
            }
            else if (!canMove)
            {
                nextDir = new Vector3(Mathf.RoundToInt(curDir.x), curDir.y, Mathf.RoundToInt(curDir.z));
            }

            rigy.MovePosition(Vector3.Lerp(curDir, nextDir, .5f * Time.deltaTime * moveSpeed));
        }
    }

    void GetNextDir()
    {
        switch (moveDirection)
        {
            case MoveDirection.Up:
                    currentMoveDirection = MoveDirection.Up;
                    curDir.x = Mathf.Lerp(curDir.x, Mathf.RoundToInt(curDir.x), .1f);
                    nextDir = curDir + Vector3.forward;
                break;


            case MoveDirection.Down:
                    currentMoveDirection = MoveDirection.Down;
                    curDir.x = Mathf.Lerp(curDir.x, Mathf.RoundToInt(curDir.x), .1f);
                    nextDir = curDir + Vector3.back;
                break;


            case MoveDirection.Right:
                currentMoveDirection = MoveDirection.Right;
                curDir.z = Mathf.Lerp(curDir.z, Mathf.RoundToInt(curDir.z), .1f);
                nextDir = curDir + Vector3.right;
                break;


            case MoveDirection.Left:
                currentMoveDirection = MoveDirection.Left;
                curDir.z = Mathf.Lerp(curDir.z, Mathf.RoundToInt(curDir.z), .1f);
                nextDir = curDir + Vector3.left;
                break;


            default:
                Debug.Log("Error in SetNextDirection Switch");
                break;

        }
    }

    bool CheckForClearTile(Vector3 dir)
    {
        Debug.DrawLine(transform.position, transform.position + dir);
        Debug.DrawLine(transform.position, nextDir,Color.red);
        bool cango = true;

        RaycastHit hit;


        if (Physics.Linecast(transform.position,transform.position + dir,out hit))
        {
            if(hit.transform.tag == "Wall")
            { 
            cango = false;
            moveDirection = currentMoveDirection;
            }
        }
        else
        {
            cango = true;
            canMove = true;
        }

        return cango;

    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Coin")
        {
            GameManager.CoinNum--;
            GameManager.gameManagerInstance.AddScore();
            Destroy(col.gameObject);
        }
        if(col.gameObject.tag == "Enemy")
        {
            GameManager.gameManagerInstance.LoseLife();
            Destroy(gameObject);
        }
    }

    public Vector3 getNextDir()
    {
        return nextDir;
    }

    public void Shoot()
    {
        float bulletDir = 0;
        switch (moveDirection)
        {
            case MoveDirection.Up:
                bulletDir = 0;
                break;
            case MoveDirection.Down:
                bulletDir = 180;
                break;


            case MoveDirection.Right:
                bulletDir = 90;
                break;
            case MoveDirection.Left:
                bulletDir = 270;
                break;
            default:
                Debug.Log("Error in SetNextDirection Switch");
                break;

        }
        GameObject bulletClone = bullet;
        bulletClone = Instantiate(bulletClone, transform.position, Quaternion.Euler(90,bulletDir,0)) as GameObject;
    }
}
