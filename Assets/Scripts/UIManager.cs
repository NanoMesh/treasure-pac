﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public static UIManager uiManagerInstance;
    public static int UIState;
    /*
      UIState
      0 = MainMenu 
      1 = HowToPlay
      2 = Pause
      3 = Play
      4 = Victory Screen
      5 = GameOverScreeen
      6 = Loading Screen
         */
     /*UITexts
        0 Score
        1 Darts
        2 Lives
    */
    public Text[] UITexts;
    public GameObject[] MenuPanels;
    public Scene currentScene;

    void Awake()
    {
        if (uiManagerInstance == null)
        {
            uiManagerInstance = this;
        }
        else
        {
            Destroy(this);
        }

        DontDestroyOnLoad(this);

    }

    void Start()
    {
        UpdateUI();
    }

    public void UpdateUI()
    {
        UITexts[0].text = "SCORE: " + GameManager.Score;
        UITexts[1].text = "Coins: " + GameManager.CoinNum;
        UITexts[2].text = "LIVES: " + GameManager.Lives;
        for (int i = 0; i < MenuPanels.Length; i++)
        {
            MenuPanels[i].SetActive(false);
            MenuPanels[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(0,1000*i);
        }
        MenuPanels[UIState].SetActive(true);
        MenuPanels[UIState].GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);

    }

    public void SetGameState(int i)
    {
        GameManager.gamestate = i;
    }
    public void SetUIState(int i)
    {
        UIManager.UIState = i;
        UpdateUI();
    }
    public void NextLevel()
    {
        Scene nextLvl = SceneManager.GetSceneAt(currentScene.buildIndex+1);
        StartCoroutine(LoadLevel(nextLvl.ToString()));
    }
    public void LoadLevelButton(string lvlNameBut)
    {
       StartCoroutine(LoadLevel(lvlNameBut));
    }
    IEnumerator LoadLevel(string lvlName)
    {
        Debug.Log("Started Loading");
        UIManager.UIState = 6;
        UpdateUI();
        currentScene = SceneManager.GetActiveScene();
        yield return new WaitForSeconds(1f);
        //Loading
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(lvlName, LoadSceneMode.Additive);
        Debug.Log(asyncLoad.progress);
        yield return asyncLoad;
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(lvlName));
        //Unload previous scene
        yield return new WaitForSeconds(1f);
        SceneManager.UnloadScene(currentScene.name);
        currentScene = SceneManager.GetActiveScene();
        //Start the game
        Debug.Log("Loading Done, Enjoy");

        if (GameManager.gamestate == 0)
        {
            UIState = 3;
            UpdateUI();
            GameManager.gamestate = 1;
        }
        else if (GameManager.gamestate != 0)
        {
            UIState = 0;
            UpdateUI();
            GameManager.gamestate = 0;
        }
    }
}
