﻿using UnityEngine;
using System.Collections;

public class BulletFly : MonoBehaviour
{
    public float bulletSpeed = 2f;
    private Rigidbody rigy;

    void Start()
    {
        GameManager.Bullets--;
        rigy = gameObject.GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        rigy.MovePosition(Vector3.Lerp(transform.position, transform.position+transform.up, .5f * Time.deltaTime * bulletSpeed));
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Enemy")
            {
            col.gameObject.SendMessage("Dead");
            Destroy(gameObject);
            }
    
        if(col.gameObject.tag == "Wall")
            {
            Destroy(gameObject);
            }
    }

}

