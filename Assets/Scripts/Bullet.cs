﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
    void Start()
    {

        GameManager.Bullets++;
        GameManager.gameManagerInstance.bulletPos.Add(gameObject.transform.position);
        gameObject.transform.SetParent(GameObject.Find("Collactables").transform);

    }

}
