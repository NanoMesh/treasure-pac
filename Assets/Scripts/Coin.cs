﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour
{
    void Start()
    {
        
        GameManager.CoinNum++;
        GameManager.gameManagerInstance.coinPos.Add(gameObject.transform.position);
        gameObject.transform.SetParent(GameObject.Find("Collactables").transform);

    }

}
