﻿using UnityEngine;
using System.Collections;

public class GhostAI : MonoBehaviour
{
    public enum GhostType { Chaser,Ambusher,Patroller};
    public GhostType ghostType;
    public Vector3 StartPoint;
    public Vector3 EndPoint;
    public bool switchEnd;

    private NavMeshAgent navAgt;
    private GameObject target;
    private NavMeshPath pth;
    public Animator anim;

    public bool isDead;
    public float deadDelayTime;

    void Start ()
    {
        navAgt = gameObject.GetComponent<NavMeshAgent>();

        navAgt.destination = StartPoint;
    }
	
	void Update ()
    {
        if(GameManager.gamestate == 1)
        {
            navAgt.Resume();
            if (!isDead)
            {

            
                if (GameObject.FindGameObjectWithTag("Player") != null)
                {
                    target = GameObject.FindGameObjectWithTag("Player");
                }
                else
                {
                    target = this.gameObject;
            
                }
                GhostTypeSw();
            }
            else
            {
                navAgt.SetDestination(target.transform.position);
            }
            
            pth = navAgt.path;
            AnimGhost();
        }
        else
        {
            navAgt.Stop();
        }
    }

    void GhostTypeSw()
    {
        switch (ghostType)
        {
            case GhostType.Chaser:
                navAgt.SetDestination(target.transform.position);
                break;
            case GhostType.Ambusher:
                navAgt.SetDestination(target.gameObject.GetComponent<PlayerController>().getNextDir());
                break;
            case GhostType.Patroller:
                Patrol();
                break;
            default:
                Debug.Log("Error in GhostType Switch");
                break;
        }

    }

    void AnimGhost()
    {
        if (navAgt.velocity.x > 0.1f)
        {
            anim.SetFloat("animDir", 1f);
        }
        else if (navAgt.velocity.x < -0.1f)
        {
            anim.SetFloat("animDir", .6f);
        }
        else if (navAgt.velocity.z > 0.1f)
        {
            anim.SetFloat("animDir", .3f);
        }
        else if (navAgt.velocity.z < -0.1f)
        {
            anim.SetFloat("animDir", .0f);
        }
    }

    void Patrol()
    {
        
        if (navAgt.hasPath == false)
        {
            if (switchEnd)
            { 
            navAgt.SetDestination(EndPoint);
            switchEnd = false;
            }
            else
            {
            navAgt.SetDestination(StartPoint);
            switchEnd = true;
            }

        }

    }

    void OnDrawGizmos()
    {
        if(pth != null)
        { 
        for (int j = 0; j < navAgt.path.corners.Length; j++)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(navAgt.path.corners[j], .2f);
        }
        }
    }

    void Dead()
    {
        isDead = true;
        gameObject.GetComponent<Collider>().enabled = false;
        GameObject[] enemySpawns = GameObject.Find("LevelDataContainer").GetComponent<LevelData>().GetEnemySpawnPoints();
        target = enemySpawns[Random.Range(0, enemySpawns.Length)];
        StartCoroutine(DeadDelay());
    }

    IEnumerator DeadDelay()
    {
        yield return new WaitForSeconds(deadDelayTime);
        gameObject.GetComponent<Collider>().enabled = true;
        isDead = false;
    }
}
